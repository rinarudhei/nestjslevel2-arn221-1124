import { Module } from '@nestjs/common';
import { LogSiswaService } from './log-siswa.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LogSiswa } from './log-siswa.entity';

@Module({
    imports: [TypeOrmModule.forFeature([LogSiswa])],
    providers: [LogSiswaService],
    exports: [TypeOrmModule, LogSiswaService],
})
export class LogSiswaModule {}
