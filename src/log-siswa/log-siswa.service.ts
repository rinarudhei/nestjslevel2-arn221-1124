import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LogSiswa } from './log-siswa.entity';

@Injectable()
export class LogSiswaService {
    constructor(
        @InjectRepository(LogSiswa)
        private readonly logSiswaRepository: Repository<LogSiswa>,
    ) {}

    async create(logSiswa: Partial<LogSiswa>): Promise<LogSiswa> {
        try {
            return this.logSiswaRepository.save(logSiswa);
        } catch (error) {
            throw new HttpException(
                `gagal memasukan log siwa API`,
                HttpStatus.INTERNAL_SERVER_ERROR,
            );
        }
    }
}
