import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class LogSiswa {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    path: string;

    @Column()
    httpMethod: string;

    @Column()
    request: string;

    @Column()
    response: string;
}
