import { Test, TestingModule } from '@nestjs/testing';
import { LogSiswaService } from './log-siswa.service';

describe('LogSiswaService', () => {
    let service: LogSiswaService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [LogSiswaService],
        }).compile();

        service = module.get<LogSiswaService>(LogSiswaService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
