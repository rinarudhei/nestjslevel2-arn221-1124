import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class LogKelas {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    path: string;

    @Column()
    httpMethod: string;

    @Column()
    request: string;

    @Column()
    response: string;
}
