import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LogKelas } from './log-kelas.entity';

@Injectable()
export class LogKelasService {
    constructor(
        @InjectRepository(LogKelas)
        private readonly logKelasRepository: Repository<LogKelas>,
    ) {}

    async create(logKelas: Partial<LogKelas>): Promise<LogKelas> {
        try {
            return this.logKelasRepository.save(logKelas);
        } catch (error) {
            throw new HttpException(
                `gagal memasukan log kelas API`,
                HttpStatus.INTERNAL_SERVER_ERROR,
            );
        }
    }
}
