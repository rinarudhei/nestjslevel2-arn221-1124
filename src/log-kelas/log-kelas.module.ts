import { Module } from '@nestjs/common';
import { LogKelasService } from './log-kelas.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LogKelas } from './log-kelas.entity';

@Module({
    imports: [TypeOrmModule.forFeature([LogKelas])],
    providers: [LogKelasService],
    exports: [TypeOrmModule, LogKelasService],
})
export class LogKelasModule {}
