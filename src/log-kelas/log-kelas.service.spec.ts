import { Test, TestingModule } from '@nestjs/testing';
import { LogKelasService } from './log-kelas.service';

describe('LogKelasService', () => {
    let service: LogKelasService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [LogKelasService],
        }).compile();

        service = module.get<LogKelasService>(LogKelasService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
