import { Test, TestingModule } from '@nestjs/testing';
import { EventKelasController } from './event-kelas.controller';

describe('EventKelasController', () => {
    let controller: EventKelasController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [EventKelasController],
        }).compile();

        controller = module.get<EventKelasController>(EventKelasController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
