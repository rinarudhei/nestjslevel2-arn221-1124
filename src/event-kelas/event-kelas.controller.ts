import {
    Body,
    Controller,
    Delete,
    Param,
    Post,
    Put,
    UseGuards,
} from '@nestjs/common';
import { EventKelasService } from './event-kelas.service';
import { CreateKelasDto } from '../kelas/dto/create-kelas.dto';
import { UpdateKelasDto } from '../kelas/dto/update-kelas.dto';
import { AuthGuard } from 'src/auth/auth.guard';
import { Roles } from 'src/auth/roles.decorator';
import { Role } from 'src/auth/roles.enum';
import { RoleGuard } from 'src/auth/roles.guard';

@Controller('event-kelas')
export class EventKelasController {
    constructor(private readonly eventKelasService: EventKelasService) {}

    @Roles(Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.eventKelasService.remove(id);
    }

    @Roles(Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Post()
    create(@Body() createKelasDto: CreateKelasDto) {
        return this.eventKelasService.create(createKelasDto);
    }

    @Roles(Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Put(':id')
    update(@Param('id') id: number, @Body() updateKelasDto: UpdateKelasDto) {
        return this.eventKelasService.update(id, updateKelasDto);
    }
}
