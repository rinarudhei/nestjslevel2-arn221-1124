import { Injectable } from '@nestjs/common';
import { ProducerService } from 'src/kafka/producer.service';
import { Kelas } from 'src/kelas/kelas.entity';
import { kafkaTopics } from '../kafka/constants';
import { kelasEventType } from 'src/kafka/event-type/kelas-event.type';

@Injectable()
export class EventKelasService {
    constructor(private readonly producerService: ProducerService) {}

    create(kelas: Partial<Kelas>) {
        try {
            this.producerService.produce(
                kafkaTopics.INSERT_KELAS_TOPIC,
                kelasEventType.toBuffer(kelas),
            );

        } catch (error) {
            console.log('produce event insert kelas failed ' + error);
        }

        return {
            kelas,
        };
    }

    remove(id: number) {
        try{
            this.producerService.produce(
                kafkaTopics.REMOVE_KELAS_TOPIC,
                Buffer.from(id.toString()),
            );

        } catch (error) {

            console.log('produce event remove kelas failed ' + error);
        }

        return {
            id,
        };
    }

    update(id: number, updatedKelas: Partial<Kelas>) {
        try {
            updatedKelas.id = id;
            this.producerService.produce(
                kafkaTopics.UPDATE_KELAS_TOPIC,
                kelasEventType.toBuffer(updatedKelas),
            );

        } catch (error) {
            console.log('produce event update kelas failed ' + error);
        }

        return {
            id,
        };
    }
}
