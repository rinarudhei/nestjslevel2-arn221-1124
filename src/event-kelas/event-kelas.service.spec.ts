import { Test, TestingModule } from '@nestjs/testing';
import { EventKelasService } from './event-kelas.service';

describe('EventKelasService', () => {
    let service: EventKelasService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [EventKelasService],
        }).compile();

        service = module.get<EventKelasService>(EventKelasService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
