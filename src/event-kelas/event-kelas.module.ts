import { Module } from '@nestjs/common';
import { EventKelasController } from './event-kelas.controller';
import { EventKelasService } from './event-kelas.service';
import { KafkaModule } from 'src/kafka/kafka.module';

@Module({
    controllers: [EventKelasController],
    providers: [EventKelasService],
    imports: [KafkaModule],
})
export class EventKelasModule {}
