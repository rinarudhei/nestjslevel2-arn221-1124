import { Kelas } from './../kelas/kelas.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Siswa {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nama: string;

    @Column()
    tingkat: string;

    @Column()
    tahunMasuk: number;

    @Column()
    status: string;

    @ManyToOne(() => Kelas, (kelas) => kelas.siswas)
    kelas: Kelas;
}
