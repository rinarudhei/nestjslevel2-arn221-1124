import { Module } from '@nestjs/common';
import { SiswaController } from './siswa.controller';
import { SiswaService } from './siswa.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Siswa } from './siswa.entity';
import { KafkaModule } from 'src/kafka/kafka.module';

@Module({
    imports: [TypeOrmModule.forFeature([Siswa]), KafkaModule],
    controllers: [SiswaController],
    providers: [SiswaService],
    exports: [TypeOrmModule],
})
export class SiswaModule {}
