import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Siswa } from './siswa.entity';
import { DeleteResult, Equal, Like, Repository } from 'typeorm';
import { LogSiswa } from 'src/log-siswa/log-siswa.entity';
import { kafkaTopics } from '../kafka/constants';
import { ProducerService } from 'src/kafka/producer.service';
import LogEventType from '../kafka/event-type/log-event.type';

@Injectable()
export class SiswaService {
    constructor(
        @InjectRepository(Siswa)
        private readonly siswaRepository: Repository<Siswa>,
        private readonly producerService: ProducerService,
    ) {}

    log(logSiswa: Partial<LogSiswa>): void {
        const topic = kafkaTopics.SISWA_API_EVENTLOG_TOPIC;
        const event: Partial<LogSiswa> = {
            path: logSiswa.path,
            httpMethod: logSiswa.httpMethod,
            request: logSiswa.request,
            response: logSiswa.response,
        };
        this.producerService.produce(topic, LogEventType.toBuffer(event));
    }

    async create(siswa: Partial<Siswa>): Promise<Siswa> {
        const response = await this.siswaRepository.save(siswa).catch((_) => {
            throw new HttpException(
                `gagal memasukan data Siswa`,
                HttpStatus.INTERNAL_SERVER_ERROR,
            );
        });

        this.log({
            path: '/siswa',
            httpMethod: 'POST',
            request: JSON.stringify(siswa),
            response: JSON.stringify(response),
        });

        return response;
    }

    async remove(id: number): Promise<DeleteResult> {
        const siswa = await this.findById(id);
        if (!siswa) {
            throw new HttpException(
                `data siswa dengan id ${id} tidak ditemukan`,
                HttpStatus.NOT_FOUND,
            );
        }

        const response = this.siswaRepository.delete(id);

        this.log({
            path: `/siswa/${id}`,
            httpMethod: 'DELETE',
            request: JSON.stringify(siswa),
            response: JSON.stringify(response),
        });

        return response;
    }

    async update(
        id: number,
        updatedSiswa: Partial<Siswa>,
    ): Promise<Partial<Siswa>> {
        const siswa = await this.findById(id);
        if (!siswa) {
            throw new HttpException(
                `data siswa dengan id ${id} tidak ditemukan`,
                HttpStatus.NOT_FOUND,
            );
        }
        const response = await this.siswaRepository.update(id, updatedSiswa);

        this.log({
            path: `/siswa/${id}`,
            httpMethod: 'PUT',
            request: JSON.stringify(updatedSiswa),
            response: JSON.stringify(response),
        });

        return updatedSiswa;
    }

    async findAll(
        nama: string,
        tingkat: string,
        status: string,
    ): Promise<Siswa[] | null> {
        const filterWhere: any = {};
        if (nama && nama !== null && nama !== '') {
            filterWhere.nama = Like(`%${nama}%`);
        }

        if (tingkat && tingkat !== null && tingkat !== '') {
            filterWhere.tingkat = Like(`%${tingkat}%`);
        }

        if (status && status !== null && status !== '') {
            filterWhere.status = Like(`%${status}%`);
        }

        const result = await this.siswaRepository.find({
            relations: {
                kelas: true,
            },
            where: filterWhere,
        });

        if (!result || result.length === 0) {
            throw new HttpException(
                'data siswa tidak ditemukan',
                HttpStatus.NOT_FOUND,
            );
        }

        return result;
    }

    async findById(id: number): Promise<Siswa | null> {
        const result = await this.siswaRepository.findOne({
            where: {
                id: Equal(id),
            },
            relations: {
                kelas: true,
            },
        });

        if (!result) {
            throw new HttpException(
                `data siswa dengan id ${id} tidak ditemukan`,
                HttpStatus.NOT_FOUND,
            );
        }

        return result;
    }
}
