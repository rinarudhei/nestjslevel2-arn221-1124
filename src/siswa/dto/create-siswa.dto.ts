import { IsNotEmpty, IsNumber, IsString, MaxLength } from 'class-validator';
import { Kelas } from 'src/kelas/kelas.entity';

export class CreateSiswaDto {
    @IsNotEmpty()
    @IsString()
    @MaxLength(50)
    readonly nama: string;

    @IsNotEmpty()
    @IsString()
    @MaxLength(50)
    readonly tingkat: string;

    @IsNotEmpty()
    @IsNumber()
    readonly tahunMasuk: number;

    @IsNotEmpty()
    @IsString()
    readonly status: string;

    @IsNotEmpty()
    readonly kelas: Kelas;
}
