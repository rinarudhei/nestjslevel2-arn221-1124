import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    Query,
    UseGuards,
} from '@nestjs/common';
import { SiswaService } from './siswa.service';
import { CreateSiswaDto } from './dto/create-siswa.dto';
import { UpdateSiswaDto } from './dto/update-siswa.dto';
import { Role } from 'src/auth/roles.enum';
import { AuthGuard } from 'src/auth/auth.guard';
import { RoleGuard } from 'src/auth/roles.guard';
import { Roles } from 'src/auth/roles.decorator';

@Controller('siswa')
export class SiswaController {
    constructor(private readonly siswaService: SiswaService) {}

    @Roles(Role.User, Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Get()
    findAll(
        @Query('nama') nama: string,
        @Query('tingkat') tingkat: string,
        @Query('status') status: string,
    ) {
        return this.siswaService.findAll(nama, tingkat, status);
    }

    @Roles(Role.User, Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Get(':id')
    findById(@Param('id') id: number) {
        return this.siswaService.findById(id);
    }

    @Roles(Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.siswaService.remove(id);
    }

    @Roles(Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Post()
    create(@Body() createSiswaDto: CreateSiswaDto) {
        return this.siswaService.create(createSiswaDto);
    }

    @Roles(Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Put(':id')
    update(@Param('id') id: number, @Body() updateSiswaDto: UpdateSiswaDto) {
        return this.siswaService.update(id, updateSiswaDto);
    }
}
