import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

export class SignInDto {
    @IsNotEmpty()
    @IsEmail()
    @MinLength(5)
    email: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(5)
    password: string;
}
