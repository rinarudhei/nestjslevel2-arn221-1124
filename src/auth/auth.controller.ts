import {
    Body,
    Controller,
    Get,
    Post,
    Request,
    UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { SignInDto } from './dto/signin.dto';
import { AuthGuard } from './auth.guard';
import { Roles } from './roles.decorator';
import { Role } from './roles.enum';
import { RoleGuard } from './roles.guard';

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) {}

    @Post('login')
    signIn(@Body() signinDto: SignInDto) {
        return this.authService.signIn(signinDto.email, signinDto.password);
    }

    @Roles(Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Get('profile')
    ketProfile(@Request() req: any) {
        return req.user;
    }
}
