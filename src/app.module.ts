import { Module } from '@nestjs/common';
import { SiswaModule } from './siswa/siswa.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KelasModule } from './kelas/kelas.module';
import { SiswaController } from './siswa/siswa.controller';
import { KelasController } from './kelas/kelas.controller';
import { SiswaService } from './siswa/siswa.service';
import { KelasService } from './kelas/kelas.service';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { AuthController } from './auth/auth.controller';
import { AuthService } from './auth/auth.service';
import { UserService } from './user/user.service';
import { UserController } from './user/user.controller';
import { KafkaModule } from './kafka/kafka.module';
import { KelasTestConsumer } from './kafka/kelas-test.consumer';
import { LogSiswaModule } from './log-siswa/log-siswa.module';
import { LogKelasModule } from './log-kelas/log-kelas.module';
import { KelasApiEventlogConsumer } from './kafka/kelas-api-eventlog.consumer';
import { SiswaApiEventlogConsumer } from './kafka/siswa-api-eventlog.consumer';
import { EventKelasModule } from './event-kelas/event-kelas.module';
import { EventSiswaModule } from './event-siswa/event-siswa.module';
import { InsertKelasConsumer } from './kafka/insert-kelas.consumer';
import { UpdateKelasConsumer } from './kafka/update-kelas.consumer';
import { RemoveKelasConsumer } from './kafka/remove-kelas.consumer';
import { InsertSiswaConsumer } from './kafka/insert-siswa.consumer';
import { UpdateSiswaConsumer } from './kafka/update-siswa.consumer';
import { RemoveSiswaConsumer } from './kafka/remove-siswa.consumer';
import { EventKelasController } from './event-kelas/event-kelas.controller';
import { EventSiswaController } from './event-siswa/event-siswa.controller';
import { EventKelasService } from './event-kelas/event-kelas.service';
import { EventSiswaService } from './event-siswa/event-siswa.service';

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'postgres',
            host: 'localhost',
            port: 5432,
            username: 'postgres_user',
            password: 'postgres_password',
            autoLoadEntities: true,
            database: 'sekolah',
            synchronize: true,
            logging: true,
        }),
        SiswaModule,
        KelasModule,
        UserModule,
        AuthModule,
        KafkaModule,
        LogSiswaModule,
        LogKelasModule,
        EventKelasModule,
        EventSiswaModule,
    ],
    controllers: [
        SiswaController,
        KelasController,
        AuthController,
        UserController,
        EventKelasController,
        EventSiswaController,
    ],
    providers: [
        SiswaService,
        KelasService,
        AuthService,
        UserService,
        KelasTestConsumer,
        KelasApiEventlogConsumer,
        SiswaApiEventlogConsumer,
        InsertKelasConsumer,
        UpdateKelasConsumer,
        RemoveKelasConsumer,
        InsertSiswaConsumer,
        UpdateSiswaConsumer,
        RemoveSiswaConsumer,
        EventKelasService,
        EventSiswaService,
    ],
})
export class AppModule {}
