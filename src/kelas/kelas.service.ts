import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Equal, Like, Repository } from 'typeorm';
import { Kelas } from './kelas.entity';
import { ProducerService } from 'src/kafka/producer.service';
import { LogKelas } from 'src/log-kelas/log-kelas.entity';
import { kafkaTopics } from '../kafka/constants';
import LogEventType from '../kafka/event-type/log-event.type';

@Injectable()
export class KelasService {
    constructor(
        @InjectRepository(Kelas)
        private readonly kelasRepository: Repository<Kelas>,
        private readonly producerService: ProducerService,
    ) {}

    log(logKelas: Partial<LogKelas>): void {
        const topic = kafkaTopics.KELAS_API_EVENTLOG_TOPIC;
        const event: Partial<LogKelas> = {
            path: logKelas.path,
            httpMethod: logKelas.httpMethod,
            request: logKelas.request,
            response: logKelas.response,
        };
        this.producerService.produce(topic, LogEventType.toBuffer(event));
    }

    async create(kelas: Partial<Kelas>): Promise<Kelas> {
        try {
            const response = await this.kelasRepository.save(kelas);

            this.log({
                path: '/kelas',
                httpMethod: 'POST',
                request: JSON.stringify(kelas),
                response: JSON.stringify(response),
            });

            return response;
        } catch (error) {
            throw new HttpException(
                `gagal memasukan data Kelas`,
                HttpStatus.INTERNAL_SERVER_ERROR,
            );
        }
    }

    async remove(id: number): Promise<DeleteResult> {
        try {
            const kelas = await this.findById(id);
            if (!kelas) {
                throw new HttpException(
                    `data kelas dengan id ${id} tidak ditemukan`,
                    HttpStatus.NOT_FOUND,
                );
            }

            const response = this.kelasRepository.delete(id);

            this.log({
                path: `/kelas/${id}`,
                httpMethod: 'DELETE',
                request: JSON.stringify(kelas),
                response: JSON.stringify(response),
            });

            return response;
        } catch (error) {
            if (error instanceof HttpException) {
                throw error;
            }

            throw new HttpException(
                `gagal menghapus data kelas dengan id ${id}`,
                HttpStatus.INTERNAL_SERVER_ERROR,
            );
        }
    }

    async update(
        id: number,
        updatedKelas: Partial<Kelas>,
    ): Promise<Partial<Kelas>> {
        try {
            const kelas = await this.findById(id);
            if (!kelas) {
                throw new HttpException(
                    `data kelas dengan id ${id} tidak ditemukan`,
                    HttpStatus.NOT_FOUND,
                );
            }
            const response = await this.kelasRepository.update(
                id,
                updatedKelas,
            );

            this.log({
                path: `/kelas/${id}`,
                httpMethod: 'PUT',
                request: JSON.stringify(kelas),
                response: JSON.stringify(response),
            });
            return updatedKelas;
        } catch (error) {
            if (error instanceof HttpException) {
                throw error;
            }

            throw new HttpException(
                `gagal memperbarui data kelas dengan id ${id}`,
                HttpStatus.INTERNAL_SERVER_ERROR,
            );
        }
    }

    async findAll(nama: string, waliKelas: string): Promise<Kelas[] | null> {
        this.producerService.produce(
            kafkaTopics.KELAS_TEST_TOPIC,
            Buffer.from('hello-world'),
        );
        const filterWhere: any = {};
        if (nama && nama !== null && nama !== '') {
            filterWhere.nama = Like(`%${nama}%`);
        }
        if (waliKelas && waliKelas !== null && waliKelas !== '') {
            filterWhere.waliKelas = Like(`%${waliKelas}%`);
        }

        const result = await this.kelasRepository.find({
            relations: {
                siswas: true,
            },
            where: filterWhere,
        });

        if (!result || result.length === 0) {
            throw new HttpException(
                'data kelas tidak ditemukan',
                HttpStatus.NOT_FOUND,
            );
        }

        return result;
    }

    async findById(id: number): Promise<Kelas | null> {
        const result = await this.kelasRepository.findOne({
            where: {
                id: Equal(id),
            },
            relations: {
                siswas: true,
            },
        });

        if (!result) {
            throw new HttpException(
                `data kelas dengan id ${id} tidak ditemukan`,
                HttpStatus.NOT_FOUND,
            );
        }

        return result;
    }
}
