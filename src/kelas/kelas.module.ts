import { Module } from '@nestjs/common';
import { KelasController } from './kelas.controller';
import { KelasService } from './kelas.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Kelas } from './kelas.entity';
import { KafkaModule } from 'src/kafka/kafka.module';

@Module({
    imports: [TypeOrmModule.forFeature([Kelas]), KafkaModule],
    controllers: [KelasController],
    providers: [KelasService],
    exports: [TypeOrmModule],
})
export class KelasModule {}
