import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    Query,
    UseGuards,
} from '@nestjs/common';
import { KelasService } from './kelas.service';
import { CreateKelasDto } from './dto/create-kelas.dto';
import { UpdateKelasDto } from './dto/update-kelas.dto';
import { AuthGuard } from 'src/auth/auth.guard';
import { Roles } from 'src/auth/roles.decorator';
import { Role } from 'src/auth/roles.enum';
import { RoleGuard } from 'src/auth/roles.guard';

@Controller('kelas')
export class KelasController {
    constructor(private readonly kelasService: KelasService) {}

    @Roles(Role.User, Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Get()
    async findAll(
        @Query('nama') nama: string,
        @Query('waliKelas') waliKelas: string,
    ) {
        return this.kelasService.findAll(nama, waliKelas);
    }

    @Roles(Role.User, Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Get(':id')
    findById(@Param('id') id: number) {
        return this.kelasService.findById(id);
    }

    @Roles(Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.kelasService.remove(id);
    }

    @Roles(Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Post()
    create(@Body() createKelasDto: CreateKelasDto) {
        return this.kelasService.create(createKelasDto);
    }

    @Roles(Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Put(':id')
    update(@Param('id') id: number, @Body() updateKelasDto: UpdateKelasDto) {
        return this.kelasService.update(id, updateKelasDto);
    }
}
