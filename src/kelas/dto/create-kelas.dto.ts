import { IsNotEmpty, IsString } from 'class-validator';
import { Siswa } from 'src/siswa/siswa.entity';

export class CreateKelasDto {
    @IsNotEmpty()
    @IsString()
    readonly nama: string;

    @IsNotEmpty()
    @IsString()
    readonly waliKelas: string;

    readonly siswas: Siswa[];
}
