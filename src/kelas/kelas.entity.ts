import { Siswa } from 'src/siswa/siswa.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Kelas {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nama: string;

    @Column()
    waliKelas: string;

    @OneToMany(() => Siswa, (siswa) => siswa.kelas)
    siswas: Siswa[];
}
