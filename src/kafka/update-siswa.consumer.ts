import { Injectable, OnModuleInit } from '@nestjs/common';
import { ConsumerService, KafkaThrottleConfig } from './consumer.service';
import Kafka from 'node-rdkafka';
import { kafkaTopics } from './constants';
import { siswaEventType } from './event-type/siswa-event.type';
import { SiswaService } from 'src/siswa/siswa.service';
import { Siswa } from 'src/siswa/siswa.entity';

@Injectable()
export class UpdateSiswaConsumer implements OnModuleInit {
    constructor(
        private readonly consumerService: ConsumerService,
        private readonly siswaService: SiswaService,
    ) {}

    onModuleInit() {
        const throttleConfig: KafkaThrottleConfig = {
            interval: 60000,
            count: 12,
        }
        this.consumerService.consume(
            kafkaTopics.UPDATE_SISWA_TOPIC,
            async (message: Kafka.Message) => {
                console.log('consuming update siswa message');
                const value: Siswa = siswaEventType.fromBuffer(
                    message.value as Buffer,
                );
                console.log({
                    value,
                    topic: message.topic.toString(),
                    partition: message.partition.toString(),
                });
                const id = value.id;
                const updatedSiswa: Partial<Siswa> = {
                    nama: value.nama,
                    tingkat: value.tingkat,
                    tahunMasuk: value.tahunMasuk,
                    status: value.status,
                    kelas: value.kelas,
                };
                try {
                    await this.siswaService.update(id, updatedSiswa);
                } catch (error) {
                    console.log(
                        `error consume topic ${kafkaTopics.UPDATE_SISWA_TOPIC} with id ${id}`,
                    );
                }
            }, throttleConfig,
        );
    }
}
