import { Injectable, OnModuleInit } from '@nestjs/common';
import { ConsumerService } from './consumer.service';
import { kafkaTopics } from './constants';
import Kafka = require('node-rdkafka');
import { LogKelasService } from 'src/log-kelas/log-kelas.service';
import LogEventType from './event-type/log-event.type';

@Injectable()
export class KelasApiEventlogConsumer implements OnModuleInit {
    constructor(
        private readonly consumerService: ConsumerService,
        private readonly logKelasService: LogKelasService,
    ) {}

    onModuleInit() {
        this.consumerService.consume(
            kafkaTopics.KELAS_API_EVENTLOG_TOPIC,
            (message: Kafka.Message) => {
                const value = LogEventType.fromBuffer(message.value as Buffer);
                console.log({
                    value,
                    topic: message.topic.toString(),
                    partition: message.partition.toString(),
                });

                this.logKelasService.create(value);
            },
        );
    }
}
