import { Injectable } from '@nestjs/common';
import Kafka = require('node-rdkafka');

@Injectable()
export class ProducerService {
    produce(topic: string, messageBuff: Buffer) {
        console.log('writin message to the stream ...' + topic);
        const stream: Kafka.ProducerStream = Kafka.createWriteStream(
            {
                'metadata.broker.list': 'localhost:29092',
            },
            {},
            { topic },
        );

        const success = stream.write(messageBuff);

        if (success) {
            console.log('writing message to stream success');
        } else {
            console.log('writing message to stream failed');
        }
    }
}
