import { Injectable, OnModuleInit } from '@nestjs/common';
import { ConsumerService, KafkaThrottleConfig } from './consumer.service';
import Kafka from 'node-rdkafka';
import { kafkaTopics } from './constants';
import { kelasEventType } from './event-type/kelas-event.type';
import { KelasService } from 'src/kelas/kelas.service';
import { Kelas } from 'src/kelas/kelas.entity';

@Injectable()
export class InsertKelasConsumer implements OnModuleInit {
    constructor(
        private readonly consumerService: ConsumerService,
        private readonly kelasService: KelasService,
    ) {}

    onModuleInit() {
        const throttleConfig: KafkaThrottleConfig = {
            interval: 60000,
            count: 12,
        }
        this.consumerService.consume(
            kafkaTopics.INSERT_KELAS_TOPIC,
            async (message: Kafka.Message) => {
                const value: Kelas = kelasEventType.fromBuffer(
                    message.value as Buffer,
                );
                console.log({
                    value,
                    topic: message.topic.toString(),
                    partition: message.partition.toString(),
                });

                try {
                    await this.kelasService.create(value);
                } catch (error) {
                    console.log(
                        `error consuming topic ${kafkaTopics.INSERT_KELAS_TOPIC} with id ${value.id}`,
                    );
                }
            }, throttleConfig
        );
    }
}
