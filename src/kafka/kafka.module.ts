import { Module } from '@nestjs/common';
import { ProducerService } from './producer.service';
import { ConsumerService } from './consumer.service';
import { LogKelasModule } from 'src/log-kelas/log-kelas.module';

@Module({
    providers: [ProducerService, ConsumerService],
    exports: [ProducerService, ConsumerService],
    imports: [LogKelasModule],
})
export class KafkaModule {}
