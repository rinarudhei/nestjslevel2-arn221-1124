import { Injectable, OnModuleInit } from '@nestjs/common';
import { ConsumerService } from './consumer.service';
import Kafka from 'node-rdkafka';
import { kafkaTopics } from './constants';
import { SiswaService } from 'src/siswa/siswa.service';

@Injectable()
export class RemoveSiswaConsumer implements OnModuleInit {
    constructor(
        private readonly consumerService: ConsumerService,
        private readonly siswaService: SiswaService,
    ) {}

    onModuleInit() {
        this.consumerService.consume(
            kafkaTopics.REMOVE_SISWA_TOPIC,
            async (message: Kafka.Message) => {
                const value = Number(message.value?.toString());
                console.log({
                    value,
                    topic: message.topic.toString(),
                    partition: message.partition.toString(),
                });

                try {
                    await this.siswaService.remove(value);
                } catch (error) {
                    console.log('error consume remove siswa topic id ' + value);
                    console.log(error);
                }
            },
        );
    }
}
