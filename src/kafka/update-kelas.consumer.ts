import { Injectable, OnModuleInit } from '@nestjs/common';
import { ConsumerService, KafkaThrottleConfig } from './consumer.service';
import Kafka from 'node-rdkafka';
import { kafkaTopics } from './constants';
import { kelasEventType } from './event-type/kelas-event.type';
import { KelasService } from 'src/kelas/kelas.service';
import { Kelas } from 'src/kelas/kelas.entity';

@Injectable()
export class UpdateKelasConsumer implements OnModuleInit {
    constructor(
        private readonly consumerService: ConsumerService,
        private readonly kelasService: KelasService,
    ) {}

    onModuleInit() {
        const throttleConfig: KafkaThrottleConfig = {
            interval: 60000,
            count: 12,
        }
        this.consumerService.consume(
            kafkaTopics.UPDATE_KELAS_TOPIC,
            async (message: Kafka.Message) => {
                const value: Kelas = kelasEventType.fromBuffer(
                    message.value as Buffer,
                );
                console.log({
                    value,
                    topic: message.topic.toString(),
                    partition: message.partition.toString(),
                });
                const id = value.id;
                const updatedKelas: Partial<Kelas> = {
                    nama: value.nama,
                    waliKelas: value.waliKelas,
                    siswas: value.siswas,
                };
                try {
                    await this.kelasService.update(id, updatedKelas);
                } catch (error) {
                    console.log(
                        `error consume topic ${kafkaTopics.UPDATE_KELAS_TOPIC} with id ${id}`,
                    );
                }
            }, throttleConfig,
        );
    }
}
