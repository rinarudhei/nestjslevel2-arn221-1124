import { Injectable, OnApplicationShutdown } from '@nestjs/common';
import Kafka = require('node-rdkafka');

export type KafkaThrottleConfig = {
    interval: number,
    count: number,
}

@Injectable()
export class ConsumerService implements OnApplicationShutdown {
    private readonly consumers: Kafka.KafkaConsumer[] = [];

    onApplicationShutdown() {
        for (const consumer of this.consumers) {
            consumer.disconnect();
        }
    }

    consume(topic: string, cb: (data: Kafka.Message) => void, throttleConf?: KafkaThrottleConfig) {
        console.log('consuming topic ' + topic);
        const consumer = new Kafka.KafkaConsumer(
            {
                'group.id': 'school-kafka',
                'metadata.broker.list': 'localhost:29092',
            },
            {},
        );

        consumer.connect();
        try {
            consumer
                .on('ready', () => {
                    consumer.subscribe([topic]);
                    this.consumers.push(consumer);

                    if (throttleConf) {
                        console.log(`consume kafka topic ${topic} with throttle`);
                        setInterval(() => {
                            consumer.consume(throttleConf.count);
                        }, throttleConf.interval);

                    } else {
                        console.log(`consume kafka topic ${topic} without throttle`);
                        consumer.consume();
                    }
                })
                .on('data', (data: Kafka.Message) => {
                    cb(data);
                })
                .on('event.error', () => {
                    console.log('error found on consumer ' + topic);
                });
        } catch (err) {
            console.log('KafkaConsumer: A problem occured when ');
            console.log(err);
        }
    }
}
