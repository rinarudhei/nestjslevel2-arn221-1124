import { Injectable, OnModuleInit } from '@nestjs/common';
import { ConsumerService } from './consumer.service';
import { kafkaTopics } from './constants';
import Kafka = require('node-rdkafka');
import LogEventType from './event-type/log-event.type';
import { LogSiswaService } from 'src/log-siswa/log-siswa.service';

@Injectable()
export class SiswaApiEventlogConsumer implements OnModuleInit {
    constructor(
        private readonly consumerService: ConsumerService,
        private readonly logSiswaService: LogSiswaService,
    ) {}

    onModuleInit() {
        this.consumerService.consume(
            kafkaTopics.SISWA_API_EVENTLOG_TOPIC,
            (message: Kafka.Message) => {
                const value = LogEventType.fromBuffer(message.value as Buffer);
                console.log({
                    value,
                    topic: message.topic.toString(),
                    partition: message.partition.toString(),
                });

                this.logSiswaService.create(value);
            },
        );
    }
}
