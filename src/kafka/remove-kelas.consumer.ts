import { Injectable, OnModuleInit } from '@nestjs/common';
import { ConsumerService } from './consumer.service';
import Kafka from 'node-rdkafka';
import { kafkaTopics } from './constants';
import { KelasService } from 'src/kelas/kelas.service';

@Injectable()
export class RemoveKelasConsumer implements OnModuleInit {
    constructor(
        private readonly consumerService: ConsumerService,
        private readonly kelasService: KelasService,
    ) {}

    onModuleInit() {
        this.consumerService.consume(
            kafkaTopics.REMOVE_KELAS_TOPIC,
            async (message: Kafka.Message) => {
                const value = Number(message.value?.toString());
                console.log({
                    value,
                    topic: message.topic.toString(),
                    partition: message.partition.toString(),
                });

                try {
                    await this.kelasService.remove(value);
                } catch (error) {
                    console.log('error consume remove kelas topic id ' + value);
                }
            },
        );
    }
}
