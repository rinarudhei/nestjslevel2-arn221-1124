import avro = require('avsc');

export const siswaEventType = avro.Type.forSchema({
    type: 'record',
    namespace: 'sekolah.service.avro.schema',
    name: 'Siswa',
    fields: [
        { name: 'id', type: ['null', 'long'], default: null},
        { name: 'nama', type: 'string' },
        { name: 'tingkat', type: 'string' },
        { name: 'tahunMasuk', type: 'long' },
        { name: 'status', type: 'string' },
        {
            name: 'kelas',
            type: {
                type: 'record',
                name: 'SiswaKelas',
                fields: [{ name: 'id', type: 'long' }],
            },
        },
    ],
});
