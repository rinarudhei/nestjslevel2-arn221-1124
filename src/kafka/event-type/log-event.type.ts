import avro = require('avsc');

export default avro.Type.forSchema({
    type: 'record',
    name: 'Apilog',
    fields: [
        { name: 'path', type: 'string' },
        { name: 'httpMethod', type: 'string' },
        { name: 'request', type: 'string' },
        { name: 'response', type: 'string' },
    ],
});
