import avro = require('avsc');

export const kelasEventType = avro.Type.forSchema({
    type: 'record',
    name: 'Kelas',
    fields: [
        { name: 'id', type: ['null', 'long'], default: null },
        { name: 'nama', type: 'string' },
        { name: 'waliKelas', type: 'string' },
    ],
});
