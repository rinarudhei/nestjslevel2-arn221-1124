import { Injectable, OnModuleInit } from '@nestjs/common';
import { ConsumerService } from './consumer.service';
import Kafka = require('node-rdkafka');
import { kafkaTopics } from './constants';

@Injectable()
export class KelasTestConsumer implements OnModuleInit {
    constructor(private readonly consumerService: ConsumerService) {}

    onModuleInit() {
        this.consumerService.consume(
            kafkaTopics.KELAS_TEST_TOPIC,
            (message: Kafka.Message) => {
                console.log({
                    value: message.value?.toString(),
                    topic: message.topic.toString(),
                    partition: message.partition.toString(),
                });
            },
        );
    }
}
