export const kafkaTopics = {
    KELAS_TEST_TOPIC: 'kelas-test',
    KELAS_API_EVENTLOG_TOPIC: 'kelas-api-eventlog',
    SISWA_API_EVENTLOG_TOPIC: 'siswa-api-eventlog',
    INSERT_KELAS_TOPIC: 'insert-kelas',
    UPDATE_KELAS_TOPIC: 'update-kelas',
    REMOVE_KELAS_TOPIC: 'remove-kelas',
    INSERT_SISWA_TOPIC: 'insert-siswa',
    UPDATE_SISWA_TOPIC: 'update-siswa',
    REMOVE_SISWA_TOPIC: 'remove-siswa',
};


