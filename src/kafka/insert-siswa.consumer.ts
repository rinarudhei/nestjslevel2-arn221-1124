import { Injectable, OnModuleInit } from '@nestjs/common';
import { ConsumerService, KafkaThrottleConfig } from './consumer.service';
import Kafka from 'node-rdkafka';
import { kafkaTopics } from './constants';
import { siswaEventType } from './event-type/siswa-event.type';
import { SiswaService } from 'src/siswa/siswa.service';
import { Siswa } from 'src/siswa/siswa.entity';

@Injectable()
export class InsertSiswaConsumer implements OnModuleInit {
    constructor(
        private readonly consumerService: ConsumerService,
        private readonly siswaService: SiswaService,
    ) {}

    onModuleInit() {
        const throttleConfig: KafkaThrottleConfig = {
            interval: 60000,
            count: 12,
        }
        this.consumerService.consume(
            kafkaTopics.INSERT_SISWA_TOPIC,
            async (message: Kafka.Message) => {
                const value: Siswa = siswaEventType.fromBuffer(
                    message.value as Buffer,
                );
                console.log({
                    value,
                    topic: message.topic.toString(),
                    partition: message.partition.toString(),
                });

                try {
                    await this.siswaService.create(value);
                } catch (error) {
                    console.log(
                        `error consuming topic ${kafkaTopics.INSERT_SISWA_TOPIC} with id ${value.id}`,
                    );
                }
            }, throttleConfig,
        );
    }
}
