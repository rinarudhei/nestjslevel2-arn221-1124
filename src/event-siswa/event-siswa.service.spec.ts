import { Test, TestingModule } from '@nestjs/testing';
import { EventSiswaService } from './event-siswa.service';

describe('EventSiswaService', () => {
    let service: EventSiswaService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [EventSiswaService],
        }).compile();

        service = module.get<EventSiswaService>(EventSiswaService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
