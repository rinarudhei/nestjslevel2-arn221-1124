import { Injectable } from '@nestjs/common';
import { ProducerService } from 'src/kafka/producer.service';
import { Siswa } from 'src/siswa/siswa.entity';
import { kafkaTopics } from '../kafka/constants';
import { siswaEventType } from 'src/kafka/event-type/siswa-event.type';

@Injectable()
export class EventSiswaService {
    constructor(private readonly producerService: ProducerService) {}

    create(siswa: Partial<Siswa>) {
        try {
            this.producerService.produce(
                kafkaTopics.INSERT_SISWA_TOPIC,
                siswaEventType.toBuffer(siswa),
            );


        } catch (error) {
            console.log('produce insert siswa topic failed');
            console.log(error);
        }
        return {
            siswa,
        };
    }

    remove(id: number) {
        try {
            this.producerService.produce(
                kafkaTopics.REMOVE_SISWA_TOPIC,
                Buffer.from(id.toString()),
            );

        } catch (error) {
            console.log('produce remove siswa topic failed');
        }

        return {
            id,
        };
    }

    update(id: number, updatedSiswa: Partial<Siswa>) {
        try {
            updatedSiswa.id = id;
            this.producerService.produce(
                kafkaTopics.UPDATE_SISWA_TOPIC,
                siswaEventType.toBuffer(updatedSiswa),
            );

        } catch (error) {
            console.log('produce update siswa topic failed');
        }

        return {
            id,
        };
    }
}
