import { Module } from '@nestjs/common';
import { EventSiswaController } from './event-siswa.controller';
import { EventSiswaService } from './event-siswa.service';
import { KafkaModule } from 'src/kafka/kafka.module';

@Module({
    controllers: [EventSiswaController],
    providers: [EventSiswaService],
    imports: [KafkaModule],
})
export class EventSiswaModule {}
