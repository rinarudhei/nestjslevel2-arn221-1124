import { Test, TestingModule } from '@nestjs/testing';
import { EventSiswaController } from './event-siswa.controller';

describe('EventSiswaController', () => {
    let controller: EventSiswaController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [EventSiswaController],
        }).compile();

        controller = module.get<EventSiswaController>(EventSiswaController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
