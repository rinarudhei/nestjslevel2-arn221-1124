import {
    Body,
    Controller,
    Delete,
    Param,
    Post,
    Put,
    UseGuards,
} from '@nestjs/common';
import { EventSiswaService } from './event-siswa.service';
import { CreateSiswaDto } from '../siswa/dto/create-siswa.dto';
import { UpdateSiswaDto } from '../siswa/dto/update-siswa.dto';
import { AuthGuard } from 'src/auth/auth.guard';
import { Roles } from 'src/auth/roles.decorator';
import { Role } from 'src/auth/roles.enum';
import { RoleGuard } from 'src/auth/roles.guard';

@Controller('event-siswa')
export class EventSiswaController {
    constructor(private readonly eventSiswaService: EventSiswaService) {}

    @Roles(Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.eventSiswaService.remove(id);
    }

    @Roles(Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Post()
    create(@Body() createSiswaDto: CreateSiswaDto) {
        return this.eventSiswaService.create(createSiswaDto);
    }

    @Roles(Role.Admin)
    @UseGuards(AuthGuard, RoleGuard)
    @Put(':id')
    update(@Param('id') id: number, @Body() updateSiswaDto: UpdateSiswaDto) {
        return this.eventSiswaService.update(id, updateSiswaDto);
    }
}
