import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Equal, Repository } from 'typeorm';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
    ) {}
    async create(user: Partial<User>): Promise<Partial<User>> {
        const checkUser = await this.userRepository
            .findOne({
                where: {
                    email: Equal(user.email as string),
                },
            })
            .catch((_) => {
                throw new HttpException(
                    `gagal memasukan data user`,
                    HttpStatus.INTERNAL_SERVER_ERROR,
                );
            });

        if (checkUser) {
            throw new HttpException(
                `email sudah pernah digunakan`,
                HttpStatus.BAD_REQUEST,
            );
        }

        await this.userRepository.save(user);

        return user;
    }

    async findOne(email: string) {
        const result = await this.userRepository.findOne({
            where: {
                email: Equal(email),
            },
        });

        if (!result) {
            throw new HttpException(
                `user dengan email ${email} tidak ditemukan`,
                HttpStatus.NOT_FOUND,
            );
        }

        return result;
    }
}
